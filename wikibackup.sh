#!/bin/bash
echo "========================================="
echo "You Select Wiki Backup"
            
#echo "Scaning Last database from Jenkins Pipeline"
echo "-------------------------------------------"
curl -u shabbiralik@webelight.co.in:112a1265e0dddd5ac5b1293c3ec87f2db1 https://jenkins.webelight.co.in/job/Experiments/job/Wiki-Backup/api/json | jq .lastSuccessfulBuild > LastBuild.json
buildno=$(cat LastBuild.json |jq .number)
curl -u shabbiralik@webelight.co.in:112a1265e0dddd5ac5b1293c3ec87f2db1 https://jenkins.webelight.co.in/job/Experiments/job/Wiki-Backup/${buildno}/api/json | jq .artifacts > artifacts.json
                
wikibackupfromjenkins=$(jq -r '.[].fileName' artifacts.json) 
echo "--------------------------------------"
echo "Wiki Backup Name From Jenknins is" $wikibackupfromjenkins
echo "--------------------------------------"
#Scaning Last database Upload on AWS S3 
export AWS_PROFILE=webelight
wikibackupfromaws=$(aws s3api list-objects-v2 --bucket "backups-of-wiki" --query 'sort_by(Contents, &LastModified)[-1].Key' --output=text)
echo "--------------------------------------"
echo Wiki Backup Name From AWS S3 is $wikibackupfromaws
echo "--------------------------------------"
VAR1=$(if [[ "$wikibackupfromjenkins" = "$wikibackupfromaws" ]]; then echo "1"; else echo "0"; fi > name.txt) 
rm -rf artifacts.json LastBuild.json
                    
#Scaning Last database size Upload on AWS S3 
export AWS_PROFILE=webelight
size=$(aws s3 ls backups-of-wiki --recursive  --human-readable | sort | tail -n 1 | awk '{print $3}')
echo "--------------------------------------"
echo "Wiki Backups Size on AWS S3 is "$size
echo "--------------------------------------"
VAR2=$(if [[ $size > 0 ]]; then echo "1"; else echo "0"; fi > size.txt) 
echo $VAR2
